package serviceA

type ICache interface {
	Put([]*User) error
	Get() ([]*User, error)
}

type CompositeRepo struct {
	cache ICache
	repo  IRepository
}

func NewCompositeRepo(cache ICache, repo IRepository) *CompositeRepo {
	return &CompositeRepo{cache: cache, repo: repo}
}

func (c *CompositeRepo) GetUsers() ([]*User, error) {
	users, err := c.cache.Get()
	if err == ErrEmptyCache {
		users, err = c.repo.GetUsers()
		if err != nil {
			return nil, err
		}
		err = c.cache.Put(users)
		if err != nil {
			return nil, err
		}
	}

	return users, nil
}
