package serviceA

type IRepository interface {
	GetUsers() ([]*User, error)
}

type Service struct {
	repo IRepository
}

func NewService(repo IRepository) *Service {
	return &Service{repo: repo}
}

func (s *Service) GetUserList() ([]*User, error) {
	return s.repo.GetUsers()
}
