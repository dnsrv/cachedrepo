package serviceA

type RepoCache struct {
	cache     []*User
	repo      IRepository
	repoCalls int // not async safe - just for testing
}

func NewRepoCache(repo IRepository) *RepoCache {
	return &RepoCache{repo: repo}
}

func (r *RepoCache) GetUsers() ([]*User, error) {
	if len(r.cache) == 0 {
		r.repoCalls++
		var err error
		r.cache, err = r.repo.GetUsers()
		if err != nil {
			return nil, err
		}
	}

	return r.cache, nil
}

func (r *RepoCache) GetRepoCalls() int {
	return r.repoCalls
}
