package serviceA

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRepoFacade(t *testing.T) {
	// users
	testUsers := []*User{
		{Id: 1, Name: "John"},
		{Id: 2, Name: "Jane"},
		{Id: 3, Name: "Jack"},
	}

	// deps
	pgRepo := NewRepoPg(testUsers)
	cache := NewCacheInMem()

	// create new composite repo
	repoFacade := NewCompositeRepo(cache, pgRepo)

	// no calls to repo yet
	assert.Equal(t, 0, pgRepo.GetCalls())

	// create new service with repo
	service := NewService(repoFacade)
	users, err := service.GetUserList()
	assert.Nil(t, nil, err)
	assert.Equal(t, len(testUsers), len(users))

	// 1 call to repo
	assert.Equal(t, 1, pgRepo.GetCalls())

	// get users again, should be cached
	users, err = service.GetUserList()
	assert.Nil(t, nil, err)
	assert.Equal(t, len(testUsers), len(users))

	// still 1 call to repo
	assert.Equal(t, 1, pgRepo.GetCalls())
}
