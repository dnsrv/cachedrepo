package serviceA

import "errors"

type User struct {
	Id       int
	Name     string
	IsBanned bool
	IsCached bool
}

var ErrEmptyCache = errors.New("empty cache")
