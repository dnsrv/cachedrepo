package serviceA

type CacheInMem struct {
	users []*User
}

func NewCacheInMem() *CacheInMem {
	return &CacheInMem{}
}

func (c *CacheInMem) Put(users []*User) error {
	c.users = users
	return nil
}

func (c *CacheInMem) Get() ([]*User, error) {
	if len(c.users) == 0 {
		return nil, ErrEmptyCache
	}
	return c.users, nil
}
