package serviceA

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestService(t *testing.T) {
	// create test users
	testUsers := []*User{
		{Id: 1, Name: "John", IsBanned: false, IsCached: false},
		{Id: 2, Name: "Jane", IsBanned: false, IsCached: false},
		{Id: 3, Name: "Jack", IsBanned: false, IsCached: false},
	}

	// create new repo
	cachedRepo := NewRepoCache(NewRepoPg(testUsers))

	// create new service with repo
	service := NewService(cachedRepo)
	assert.Equal(t, 0, cachedRepo.GetRepoCalls())

	// get users
	users, err := service.GetUserList()
	assert.Nil(t, nil, err)
	assert.Equal(t, len(testUsers), len(users))
	assert.Equal(t, 1, cachedRepo.GetRepoCalls()) // 1 call to repo

	// get users again, should be cached
	users, err = service.GetUserList()
	assert.Nil(t, nil, err)
	assert.Equal(t, len(testUsers), len(users))
	assert.Equal(t, 1, cachedRepo.GetRepoCalls()) // still 1
}
