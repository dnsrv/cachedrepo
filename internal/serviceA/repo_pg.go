package serviceA

type RepoPg struct {
	users []*User
	calls int
}

func NewRepoPg(testUsers []*User) *RepoPg {
	return &RepoPg{
		users: testUsers,
	}
}

func (r *RepoPg) GetUsers() ([]*User, error) {
	r.calls++
	return r.users, nil
}

func (r *RepoPg) GetCalls() int {
	return r.calls
}
